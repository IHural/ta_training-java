package com.epam.training.student_iryna_hural.ta_trainingjava.page;

import com.epam.training.student_iryna_hural.ta_trainingjava.page.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.Urls.PASTEBIN_PAGE;

public class PastebinPage extends BasePage {

        public PastebinPage (WebDriver driver) {
                super(driver);
        }
        private final By pasteTextArea = By.id("postform-text");
        private final By pasteNameArea = By.id("postform-name");
        private final By pasteExpirationDropdown = By.id("select2-postform-expiration-container");
        private final By pasteHighlightingDropdown = By.id("select2-postform-format-container");
        private final By createNewPasteButton = By.cssSelector("button.btn.-big");
        private final By pageName = By.className("info-top");
        private final By keywordsSelector = By.cssSelector("span.kw2");

        private final String  xpathTemplate = "//li[@class='select2-results__option' and text()='%s']";


        public void enterNewPaste(String text) {
                WebElement pasteTextAreaField = wait.until(ExpectedConditions.elementToBeClickable(pasteTextArea));
                pasteTextAreaField.sendKeys(text);
        }

        public String getEnteredNewPaste() {
                WebElement pasteTextAreaField = driver.findElement(pasteTextArea);
                return pasteTextAreaField.getAttribute("value");
        }

        public void selectSyntaxHighlighting(String language) {
                WebElement syntaxHighlightingDropdown = wait.until(ExpectedConditions.visibilityOfElementLocated(pasteHighlightingDropdown));
                syntaxHighlightingDropdown.click();
                String languageXpath = String.format(xpathTemplate, language);
                WebElement languageOption = driver.findElement(By.xpath(languageXpath));
                languageOption.click();
        }
        public void selectExpiration(String option) {
                WebElement pasteExpirationDropdownField = wait.until(ExpectedConditions.visibilityOfElementLocated(pasteExpirationDropdown));
                pasteExpirationDropdownField.click();
                String expirationXpath = String.format(xpathTemplate, option);
                WebElement expirationOption = driver.findElement(By.xpath(expirationXpath));
                expirationOption.click();
        }

        public String getSelectedExpiration() {
                WebElement pasteExpirationDropdownField = driver.findElement(pasteExpirationDropdown);
                return pasteExpirationDropdownField.getText();
        }

        public void enterPasteName(String name) {
                WebElement pasteNameField = driver.findElement(pasteNameArea);
                pasteNameField.sendKeys(name);
        }

        public String getEnteredPasteName() {
                WebElement pasteNameField = driver.findElement(pasteNameArea);
                return pasteNameField.getAttribute("value");
        }

        public void clickSubmitButton() {
                WebElement submitButton = driver.findElement(createNewPasteButton);
                submitButton.click();
        }
        public String getCurrentUrl() {
                return driver.getCurrentUrl();
        }
        public String getPageName() {
                WebElement pageNameField = driver.findElement(pageName);
                return pageNameField.getText();
        }

        public boolean isCodeHighlighted() {
                List<WebElement> keywords = driver.findElements(keywordsSelector);
                for (WebElement keyword : keywords) {
                        String fontSize = keyword.getCssValue("font-size");
                        String lineHeight = keyword.getCssValue("line-height");
                        if (fontSize.equals("12px") && lineHeight.equals("24px")) {
                                return true;
                        }
                }
                return false;
        }
}