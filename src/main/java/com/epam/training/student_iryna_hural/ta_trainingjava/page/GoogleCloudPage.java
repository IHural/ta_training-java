package com.epam.training.student_iryna_hural.ta_trainingjava.page;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.Urls.GOOGLE_CLOUD;
import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.Urls.GOOGLE_CLOUD_PRICING_CALCULATOR;


public class GoogleCloudPage extends BasePage {
    public static final String PASTE_TEXT_FOR_SEARCH = "Google Cloud Platform Pricing Calculator";
    public static final String DATACENTER_LOCATION = "Frankfurt";
    public GoogleCloudPage(WebDriver driver) {
        super(driver);
    }
    public void navigateTo() {
        driver.get(GOOGLE_CLOUD);
    }


    private final By searchIcon = By.xpath(
            "//*[@id=\"kO001e\"]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div/div");
    private final By searchFieldLocator = By.cssSelector("#i4");
    private final By calculatorLink = By.xpath(
            "//a[@href='" + GOOGLE_CLOUD_PRICING_CALCULATOR +"']");

    private final By numberOfInstancesInput = By.id("input_100");
    private final By seriesDropdown = By.id("select_value_label_95");
    private final By series = By.id("select_option_224");
    private final By machineTypeDropdown = By.id("select_value_label_96");
    private final By machineType = By.id("select_option_474");
    private final By addGPULink = By.xpath(
            "//md-checkbox[@aria-label='Add GPUs']");
    private final By gpuTypeDropdown = By.id("select_510");
    private final By gpuType = By.id("select_option_518");
    private final By gpuTypeValue = By.id("select_value_label_508");
    private final By numberOfGPUsInput = By.id("select_value_label_509");
    private final By numberOfGPUs = By.id("select_option_521");
    private final By localSSD = By.id("select_value_label_468");
    private final By ssdSize = By.id("select_option_495");
    private final By datacenterLocation = By.id("select_value_label_98");
    private final By searchRegion = By.id("input_132");
    private final By location = By.id("select_option_268");
    private final By commitUsage = By.id("select_value_label_99");
    private final By usage = By.id("select_option_138");
    private final By addToEstimateButton = By.xpath(
            "//button[contains(text(), 'Add to Estimate')]");
    private final By totalEstimatedCost =By.xpath("//b[contains(text(),'Total Estimated Cost')]");


    public GoogleCloudPage searchingGoogleCloudCalculator(){
        wait.until(ExpectedConditions.elementToBeClickable(searchIcon)).click();
        WebElement searchField = wait.until(ExpectedConditions.elementToBeClickable(searchFieldLocator));
        searchField.sendKeys(PASTE_TEXT_FOR_SEARCH);
        searchField.sendKeys(Keys.ENTER);
        return this;
    }
    public GoogleCloudPage navigateToPricingCalculator (){
        wait.until(ExpectedConditions.elementToBeClickable(calculatorLink)).click();
        return this;
    }
    public GoogleCloudPage switchToCalculatorFrame() {
        WebElement frame = driver.findElement(By.tagName("iframe"));
        driver.switchTo().frame(frame);

        WebElement iFrame = driver.findElement(By.id("myFrame"));
        driver.switchTo().frame(iFrame);
        return this;
    }

    public GoogleCloudPage enterNumberOfInstances(String number) {
        wait.until(ExpectedConditions.elementToBeClickable(numberOfInstancesInput)).click();
        WebElement numberOfInstances= wait.until(ExpectedConditions.elementToBeClickable(numberOfInstancesInput));
        numberOfInstances.sendKeys(number);
        return this;
    }
    public GoogleCloudPage selectSeries() {
        WebElement seriesDropdownField = wait.until(ExpectedConditions.elementToBeClickable(seriesDropdown));
        seriesDropdownField.click();
        WebElement seriesOption = wait.until(ExpectedConditions.elementToBeClickable(series));
        seriesOption.click();
        return this;
    }

    public GoogleCloudPage selectMachineType() {
        WebElement machineTypeDropdownField = wait.until(ExpectedConditions.elementToBeClickable(machineTypeDropdown));
        machineTypeDropdownField.click();
        WebElement machineTypeOption = wait.until(ExpectedConditions.elementToBeClickable(machineType));
        machineTypeOption.click();
        return this;
    }

    public GoogleCloudPage addGPUs() {
        wait.until(ExpectedConditions.elementToBeClickable(addGPULink)).click();
        return this;
    }

    public GoogleCloudPage selectGPUType() {
        WebElement gpuTypeDropdownField = wait.until(ExpectedConditions.elementToBeClickable(gpuTypeDropdown));
        gpuTypeDropdownField.click();
        WebElement gpuTypeOption = wait.until(ExpectedConditions.elementToBeClickable(gpuType));
        gpuTypeOption.click();
        return this;
    }

    public GoogleCloudPage selectNumberOfGPUs() {
        WebElement numberOfGPUsField = wait.until(ExpectedConditions.elementToBeClickable(numberOfGPUsInput));
        numberOfGPUsField.click();
        WebElement numberOfGPUsOption = wait.until(ExpectedConditions.elementToBeClickable(numberOfGPUs));
        numberOfGPUsOption.click();
        return this;
    }

    public GoogleCloudPage selectLocalSSD() {
        WebElement localSSDField = wait.until(ExpectedConditions.elementToBeClickable(localSSD));
        localSSDField.click();
        WebElement ssdSizeOption = wait.until(ExpectedConditions.elementToBeClickable(ssdSize));
        ssdSizeOption.click();
        return this;
    }

    public GoogleCloudPage selectDatacenterLocation() {
        WebElement datacenterLocationField = wait.until(ExpectedConditions.elementToBeClickable(datacenterLocation));
        datacenterLocationField.click();
        return this;
    }
    public GoogleCloudPage searchRegionOption() {
        WebElement searchRegionField = wait.until(ExpectedConditions.elementToBeClickable(searchRegion));
        searchRegionField.click();
        searchRegionField.sendKeys(DATACENTER_LOCATION);
        WebElement locationOption = wait.until(ExpectedConditions.elementToBeClickable(location));
        locationOption.click();
        return this;
    }

    public GoogleCloudPage selectCommitmentUsage() {
        WebElement commitUsageField = wait.until(ExpectedConditions.elementToBeClickable(commitUsage));
        commitUsageField.click();
        WebElement usageOption = wait.until(ExpectedConditions.elementToBeClickable(usage));
        usageOption.click();
        return this;
    }

    public GoogleCloudPage clickAddToEstimateButton() {
        WebElement buttonAddToEstimate = wait.until(ExpectedConditions.elementToBeClickable(addToEstimateButton));
        waitElementIsVisible(buttonAddToEstimate).click();
        return this;
    }

    public String getTotalEstimatedCost() {
        WebElement totalCost = wait.until(ExpectedConditions.visibilityOfElementLocated(totalEstimatedCost));
        return totalCost.getText();
    }

    public String getSelectedNumberOfInstances() {
        WebElement selectedNumberOfInstances = driver.findElement(numberOfInstancesInput);
        return selectedNumberOfInstances.getAttribute("value").trim();
    }

    public String getSelectedSeries() {
        WebElement selectedSeriesElement = driver.findElement(seriesDropdown);
        return selectedSeriesElement.getText().trim();
    }

    public String getSelectedMachineType() {
        WebElement selectedMachineTypeElement = driver.findElement(machineTypeDropdown);
        return selectedMachineTypeElement.getText().trim();
    }

    public boolean areGPUsAdded() {
        WebElement gpuCheckbox = wait.until(ExpectedConditions.visibilityOfElementLocated(addGPULink));
        String ariaCheckedValue = gpuCheckbox.getAttribute("aria-checked");
        return ariaCheckedValue.equals("true");
    }

    public String getSelectedGPUType() {
        WebElement selectedGPUTypeElement = wait.until(ExpectedConditions.visibilityOfElementLocated(gpuTypeValue));
        return selectedGPUTypeElement.getText().trim();
    }

    public String getSelectedNumberOfGPUs() {
        WebElement selectedNumberOfGPUsElement = wait.until(ExpectedConditions.visibilityOfElementLocated(numberOfGPUsInput));
        return selectedNumberOfGPUsElement.getText().trim();
    }

    public String getSelectedLocalSSD() {
        WebElement selectedLocalSSDElement = driver.findElement(localSSD);
        return selectedLocalSSDElement.getText().trim();
    }

    public String getSelectedDatacenterLocation() {
        WebElement selectedDatacenterLocationElement = driver.findElement(datacenterLocation);
        return selectedDatacenterLocationElement.getText().trim();
    }

    public String getSelectedCommitmentUsage() {
        WebElement selectedCommitmentUsageElement = driver.findElement(commitUsage);
        return selectedCommitmentUsageElement.getText().trim();
    }
}