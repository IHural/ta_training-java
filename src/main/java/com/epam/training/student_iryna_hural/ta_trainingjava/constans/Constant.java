package com.epam.training.student_iryna_hural.ta_trainingjava.constans;

public class Constant {
        public static class TimeoutVariable {
            public static final int IMPLICIT_WAIT = 5;
            public static final int EXPLICIT_WAIT = 8;

        }
        public static class Config {
            public static final Boolean HOLD_BROWSER_OPEN = true;
        }
        public static class Urls{
            public static final String PASTEBIN_PAGE = ("https://pastebin.com/");

            public static final String GOOGLE_CLOUD = ("https://cloud.google.com/");

            public static final String GOOGLE_CLOUD_PRICING_CALCULATOR = ("https://cloud.google.com/products/calculator-legacy");

        }

    }

