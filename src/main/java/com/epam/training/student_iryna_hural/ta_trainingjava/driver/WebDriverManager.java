package com.epam.training.student_iryna_hural.ta_trainingjava.driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.concurrent.TimeUnit;

import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.TimeoutVariable.IMPLICIT_WAIT;

public class WebDriverManager {
    public static WebDriver driver;

    public static WebDriver getDriver(BrowserType browserType) {
        if (driver == null) {
            switch (browserType) {
                case CHROME:
                    System.setProperty("webdriver.chrome.driver", "/Users/dh/Downloads/chromedriver-mac-x64/chromedriver");
                    driver = new ChromeDriver();
                    break;
                case SAFARI:
                    driver = new SafariDriver();
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported browser type: " + browserType);
            }

        }
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, TimeUnit.SECONDS);
        return driver;
    }

    public static void closeDriver() {
            driver.quit();
    }
}

