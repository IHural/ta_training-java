package com.epam.training.student_iryna_hural.ta_trainingjava.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.TimeoutVariable.EXPLICIT_WAIT;
import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.TimeoutVariable.IMPLICIT_WAIT;

public class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    public BasePage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(IMPLICIT_WAIT));        }

    public void open(String url){
        driver.get(url);
    }
    public WebElement waitElementIsVisible(WebElement element){
        new WebDriverWait(driver,Duration.ofSeconds(EXPLICIT_WAIT)).until(ExpectedConditions.visibilityOf(element));
        return element;
    }
}

