package com.epam.training.student_iryna_hural.ta_trainingjava.base;

import com.epam.training.student_iryna_hural.ta_trainingjava.page.GoogleCloudPage;
import com.epam.training.student_iryna_hural.ta_trainingjava.page.PastebinPage;
import com.epam.training.student_iryna_hural.ta_trainingjava.driver.BrowserType;
import com.epam.training.student_iryna_hural.ta_trainingjava.driver.WebDriverManager;
import com.epam.training.student_iryna_hural.ta_trainingjava.page.BasePage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;



public class BaseTest {
    protected WebDriver driver;
    protected BasePage basePage;
    protected PastebinPage pastebinPage;
    protected GoogleCloudPage googleCloudPage;
    @BeforeEach
    public void setUp() {
        driver = WebDriverManager.getDriver(BrowserType.CHROME);
        basePage = new BasePage(driver);
        pastebinPage = new PastebinPage(driver);
        googleCloudPage = new GoogleCloudPage(driver);
    }
    @AfterEach
    public void tearDown() {
        if (driver != null) {
            WebDriverManager.closeDriver();
        }
    }
}
