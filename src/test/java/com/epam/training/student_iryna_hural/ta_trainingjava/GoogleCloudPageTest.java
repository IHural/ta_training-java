package com.epam.training.student_iryna_hural.ta_trainingjava;

import com.epam.training.student_iryna_hural.ta_trainingjava.base.BaseTest;
import org.junit.jupiter.api.*;


import static com.epam.training.student_iryna_hural.ta_trainingjava.TestData.OptionalPasteSettings.NUMBER_OF_INSTANCES;
import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.Urls.GOOGLE_CLOUD;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GoogleCloudPageTest extends BaseTest {

    @Test
    public void testSelectedOptions() {
        basePage.open(GOOGLE_CLOUD);
        googleCloudPage.navigateTo();

        googleCloudPage.searchingGoogleCloudCalculator()
        .navigateToPricingCalculator();

        googleCloudPage.switchToCalculatorFrame()
        .enterNumberOfInstances(NUMBER_OF_INSTANCES)
        .selectSeries()
        .selectMachineType()
        .addGPUs()
        .selectGPUType()
        .selectNumberOfGPUs()
        .selectLocalSSD()
        .selectDatacenterLocation()
        .searchRegionOption()
        .selectCommitmentUsage();


        assertEquals("4",googleCloudPage.getSelectedNumberOfInstances(),"Selected number of instances does not match expected value");
        assertEquals("N1", googleCloudPage.getSelectedSeries(), "Selected series does not match expected value");
        assertEquals("n1-standard-8 (vCPUs: 8, RAM: 30GB)", googleCloudPage.getSelectedMachineType(), "Selected machine type does not match expected value");
        assertTrue(googleCloudPage.areGPUsAdded(), "GPUs were not added as expected");
        assertEquals("NVIDIA Tesla T4", googleCloudPage.getSelectedGPUType(), "Selected GPU type does not match expected value");
        assertEquals("2", googleCloudPage.getSelectedNumberOfGPUs(), "Selected number of GPUs does not match expected value");
        assertEquals("2x375 GB", googleCloudPage.getSelectedLocalSSD(), "Selected local SSD does not match expected value");
        assertEquals("Frankfurt (europe-west3)", googleCloudPage.getSelectedDatacenterLocation(), "Selected datacenter location does not match expected value");
        assertEquals("1 Year", googleCloudPage.getSelectedCommitmentUsage(), "Selected commitment usage does not match expected value");

        googleCloudPage.clickAddToEstimateButton();
        String estimatedCost = googleCloudPage.getTotalEstimatedCost();
        assertNotNull(estimatedCost, "Estimated cost is not displayed");
        assertTrue(estimatedCost.contains("Total Estimated Cost"), "Total Estimated Cost is not present in the estimated cost");
    }
}