package com.epam.training.student_iryna_hural.ta_trainingjava;

import com.epam.training.student_iryna_hural.ta_trainingjava.base.BaseTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.epam.training.student_iryna_hural.ta_trainingjava.TestData.OptionalPasteSettings.*;
import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.Urls.PASTEBIN_PAGE;


public class PastebinPageTextTest extends BaseTest {

    @Test
    public void testPasteCreation() {
        basePage.open(PASTEBIN_PAGE);
        String pageTitle = driver.getTitle();
        Assertions.assertTrue(pageTitle.contains("Pastebin"), "Page title does not contain 'Pastebin'");

        pastebinPage.enterNewPaste(NEW_PASTE);
        String enteredText = pastebinPage.getEnteredNewPaste();
        Assertions.assertEquals("Hello from WebDriver", enteredText, "Entered text is incorrect");
        pastebinPage.selectExpiration(PASTED_EXPIRATION);
        String selectedOption = pastebinPage.getSelectedExpiration();
        Assertions.assertTrue(selectedOption.contains("10 Minutes"), "Selected expiration is incorrect");

        pastebinPage.enterPasteName(PASTE_NAME);
        String enteredName = pastebinPage.getEnteredPasteName();
        Assertions.assertEquals("helloweb", enteredName, "Entered name is incorrect");

        pastebinPage.clickSubmitButton();
        pageTitle = driver.getTitle();
        Assertions.assertTrue(pageTitle.contains("Pastebin"), "Page title after submission does not contain 'Pastebin'");
    }

}

