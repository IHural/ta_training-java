package com.epam.training.student_iryna_hural.ta_trainingjava;

public class TestData {
    public static class OptionalPasteSettings {
        public static final String NEW_PASTE = "Hello from WebDriver";
        public static final String NEW_PASTE_CODE = "git config --global user.name  \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force";
        public static final String PASTED_EXPIRATION = "10 Minutes";
        public static final String SYNTAX_HIGHLIGHTING = "Bash";
        public static final String PASTE_NAME = "helloweb";
        public static final String PASTE_NAME_1 = "how to gain dominance among developers";
        public static final String NUMBER_OF_INSTANCES = "4";




    }
}
