package com.epam.training.student_iryna_hural.ta_trainingjava;

import com.epam.training.student_iryna_hural.ta_trainingjava.base.BaseTest;
import org.junit.jupiter.api.*;

import static com.epam.training.student_iryna_hural.ta_trainingjava.TestData.OptionalPasteSettings.*;
import static com.epam.training.student_iryna_hural.ta_trainingjava.constans.Constant.Urls.PASTEBIN_PAGE;

public class PastebinPageCodeTest extends BaseTest {

        @Test
        public void testPasteCreation() {
            basePage.open(PASTEBIN_PAGE);
            pastebinPage.enterNewPaste(NEW_PASTE_CODE);
            pastebinPage.selectSyntaxHighlighting(SYNTAX_HIGHLIGHTING);
            pastebinPage.selectExpiration(PASTED_EXPIRATION);
            pastebinPage.enterPasteName(PASTE_NAME_1);
            pastebinPage.clickSubmitButton();

            String currentUrl = pastebinPage.getCurrentUrl();
            Assertions.assertTrue(currentUrl.startsWith(PASTEBIN_PAGE), "URL doesn't start with " + PASTEBIN_PAGE + ": FAIL");

            boolean isHighlighted = pastebinPage.isCodeHighlighted();
            Assertions.assertTrue(isHighlighted, "Syntax highlighting is not set to " + SYNTAX_HIGHLIGHTING +": FAIL");

            String pageName = pastebinPage.getPageName();
            Assertions.assertEquals(PASTE_NAME_1, pageName, "Title does not match paste name: FAIL");
        }

}
